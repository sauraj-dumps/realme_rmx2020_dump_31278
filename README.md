## full_oppo6769-user 10 QP1A.190711.020 68b77aba7cb33275 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: full_oppo6769-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: 0730_202203042038
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: RMX2020_11.A.73_0730_202203042038
- Branch: RMX2020_11.A.73_0730_202203042038
- Repo: realme_rmx2020_dump_31278


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
